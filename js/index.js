const growParameters = {
  "Height": "height",
  "Light Schedule": "lightScheduel",
  "Day Air Temperature": "dayAirTemp",
  "Night Air Temperature": "nightAirTemp",
  "pH": "ph",
  "TDS": "tds",
  "EC": "ec",
  "Air Humidity": "airHumidity",
  "Smell": "smell",
  "Solution Temperature": "solutionTemp",
  "Substrate Temperature": "substrateTemp",
  "Pot Size": "potSize",
  "Lamp To Plant Distance": "lampToPlantDistance",
  "Watering Volume Per Plant Per 24h": "water",
  "CO₂ Level": "co2",
};

const harvestParameters = {
  "Total time": "weekNumber",
  "# plants": "plantCount",
  "Dry weight": "dryWeight",
  "Wet weight": "wetWeight",
  "Grow room": "roomSize",
  "Total light power": "totalWatt",
  "Rating": "rating",
  "Ease of grow": "growEase",
  "Resistance": "resistance",
}

const preferedUnits = [
  "PPM",
  "watt",
  "%",
  "plants",
  "weeks",
  "mS/cm",
  "pH",
  "hrs",
  "",
];

const metricUnits = [
  "cm",
  "l",
  "°C",
  "m²",
  "g",
  "ml/l",
]

const imperialUnits = [
  "inch",
  "gal",
  "°F",
  "ft²",
  "oz",
  "tsp/gal",
]

let activatedParameters = [
  "Height",
  "Air Humidity",
  "Lamp To Plant Distance",
];

let colors = [
  '#d70206',
  '#f05b4f',
  '#f4c63d',
  '#d17905',
  '#453d3f',
  '#59922b',
  '#0544d3',
  '#6b0392',
  '#f05b4f',
  '#dda458',
  '#eacf7d',
  '#86797d',
  '#b2c326',
  '#6188e2',
  '#a748ca',
]

let fertExclude = [];

let diary = null;
let isMetricOn = true;
let pu = preferedUnits.concat(metricUnits);

function loadSummary(url, pushState){
  var diaryPromise = diaryScraper(url);

  resetData();
  
  if(pushState)
    window.history.pushState("", "", '/?url=' + url);

  diaryPromise.then(function(newDiary){
    diary = newDiary;
    updateAll();
    document.getElementById("diary-title").innerHTML = diary.title;
    document.getElementById("spinner").classList.remove("active");
    document.getElementById("results").classList.add("active");
  });
}

function resetData(){
  // Clear charts and show spinner
  document.getElementById("spinner").classList.add("inited");
  document.getElementById("spinner").classList.add("active");
  document.getElementById("results").classList.remove("active");
}

function updateAll(){
  updateParameters();
  updateFert();
  updateHarvest();
}

function updateHarvest(){
  const harvestWeeks = diary.getHarvestWeeks();
  const harvestResultElement = document.getElementById("harvest-result");

  if(harvestWeeks.length == 0)
    harvestResultElement.innerHTML = "";
  else {
    harvestResultElement.innerHTML = "<h3>Harvest</h3>";


    for(const harvestWeek of harvestWeeks){
      const strainResult = document.createElement("article");
      strainResult.classList.add("message");
      strainResult.classList.add("is-success");

      // Create a title with strain name + breeder
      const headElement = document.createElement("div");
      headElement.classList.add("message-header");
      const titleElement = document.createElement("h4");
      titleElement.classList.add("strain-harvest");
      titleElement.innerHTML = harvestWeek.getParameterValue("strainName", pu).value + " - " + harvestWeek.getParameterValue("breeder", pu).value;
      headElement.appendChild(titleElement);

      // Create table and 2 table rows
      const tableContainerElement = document.createElement("div");
      tableContainerElement.classList.add("table-container");
      const tableElement = document.createElement("table");
      tableContainerElement.appendChild(tableElement);
      tableElement.classList.add("table");

      const tr1Element = document.createElement("tr");
      const tr2Element = document.createElement("tr");

      // Add the rows to the tabel
      tableElement.appendChild(tr1Element);
      tableElement.appendChild(tr2Element);

      for(const harvestParam in harvestParameters){
        // Create a new table data elements
        const thElement = document.createElement("th");
        const tdElement = document.createElement("td");

        thElement.innerHTML = harvestParam;
        paramValue = harvestWeek.getParameterValue(harvestParameters[harvestParam], pu);
        tdElement.innerHTML = paramValue.value ? paramValue.value + " " + paramValue.unit : "-";
        tr1Element.appendChild(thElement);
        tr2Element.appendChild(tdElement);
      }

      // Calc weight/plant
      const thElement = document.createElement("th");
      const tdElement = document.createElement("td");

      plants = harvestWeek.getParameterValue("plantCount", pu);
      dryweight = harvestWeek.getParameterValue("dryWeight", pu);
      thElement.innerHTML = dryweight.unit + "/plant";
      tdElement.innerHTML = dryweight.value && plants.value ? (dryweight.value / plants.value).toFixed(2) : "-";
      tr1Element.appendChild(thElement);
      tr2Element.appendChild(tdElement);

      const strainDescriptionElement = document.createElement("p");
      strainDescriptionElement.innerHTML = "<strong>User description:</strong> " + harvestWeek.textPost;

      strainResult.appendChild(headElement);
      strainResult.appendChild(tableContainerElement);
      strainResult.appendChild(strainDescriptionElement);
      harvestResultElement.appendChild(strainResult);
    }
  }
}

function updateFert(){
  // get fert arrays and names
  const fertArrays = diary.getFertArrays(pu);
  const weekNumbers = getWeeks();

  // show names of fert and give right color
  const fertList = document.getElementById("fert-list");
  fertList.innerHTML = "";

  let seriesIndex = 1;
  for(const fert in fertArrays){
    fertElement = document.createElement("p");
    fertElement.innerHTML = fert;
    fertElement.classList.add("button");
    fertElement.onclick = () => {
      if(fertExclude.includes(fert))
        fertExclude = fertExclude.filter(item => item != fert);
      else
        fertExclude.push(fert);

      updateFert();
    }
    fertList.appendChild(fertElement);

    if(!fertExclude.includes(fert)){
      fertElement.classList.add("series-" + seriesIndex++);

      // Set the meta tag for hover info
      fertArrays[fert] = fertArrays[fert].map(item => { return item ? {'value': item.value, 'meta': item.label + " (" + item.unit + ")"} : null })
    } else 
      delete fertArrays[fert];
  }

  // draw chart
  drawChart(weekNumbers, Object.values(fertArrays), "#fert-chart", false);
}

// Toggle between metric and imperial
function switchUnits(){
  isMetricOn = !isMetricOn;
  document.getElementById("unit-button").innerHTML = isMetricOn ? "<strong>Metric</strong><sub>/Imperial</sub>" : "<sub>Metric/</sub><strong>Imperial</strong>";
  pu = preferedUnits.concat(isMetricOn ? metricUnits : imperialUnits);
  updateAll();
}

// Get an array of all the weeks
function getWeeks(){
  var weekNumbers = Array();
  for(var i = 1; i <= diary.numberOfWeeks; i++)
    weekNumbers.push("Week " + i);
  return weekNumbers;
}

// Get the parameter values from the diary and draw the chart
function updateParameters(){
  if(diary){
    let paramArrays = Array();

    for(let param of activatedParameters)
      paramArrays.push(diary.getGrowAttributeArray(growParameters[param], pu)
        .map(item => { return item ? {'value': item.value, 'meta': item.unit ? param + " (" + item.unit + ")" : param} : null }))

    const weekNumbers = getWeeks();
    drawChart(weekNumbers, paramArrays, '#param-chart', true);
  }
  updateColors();
}

// Update the colors of the parameter buttons
function updateColors(){
  colorIndex = 1;

  for(let param of activatedParameters){
    buttonElement = document.getElementById(growParameters[param]);
    removeAllSeriesClasses(buttonElement);
    buttonElement.classList.add("series-"+colorIndex++);
  }
}

// Draw the chart with animations
function drawChart(labels, series, chartId, fillHoles){
  var data = {
    // A labels array that can contain any sort of values
    labels: labels,
    // Our series array that contains series objects or in this case series data arrays
    series: series
  };

  var options = {
    fullWidth: true,
    chartPadding: {
      right: 10
    },
    lineSmooth: Chartist.Interpolation.cardinal({
      fillHoles: fillHoles,
    }),
    low: 0,
    plugins: [
      Chartist.plugins.tooltip({
        appendToBody: false,
      }),
    ]
  }

  // Create a new line chart object where as first parameter we pass in a selector
  // that is resolving to our chart container element. The Second parameter
  // is the actual data object.
  var chart = new Chartist.Line(chartId, data, options);

  // Add some animation :D
  chart.on('draw', function(data) {
    if(data.type === 'line' || data.type === 'area') {
      data.element.animate({
        d: {
          dur: 1000,
          from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
          to: data.path.clone().stringify(),
          easing: Chartist.Svg.Easing.easeOutQuint
        }
      });
    } else if(data.type === 'point'){
      data.element.animate({
        opacity: {
          begin: 600,
          dur: 800,
          from: 0,
          to: 1,
          easing: 'easeOutQuart'
        },
      });
    }
  });

  // Detach the chart from the resize listener
  chart.on('created', () => {
    chart.detach(); // it will detach resize and media query listeners
  });

  // Then update the chart only when window.width() changes
  var width = window.innerWidth;
  window.addEventListener("resize", () => {
    if(window.innerWidth != width){
      width = window.innerWidth;
      chart.update();
    }
  });
}

// Make Parameter buttons from the parameters
function initParamButtons(){
  const paramButtons = Array();
  const buttonGroup = document.getElementById("parameter-button-group");

  for(const param in growParameters){
    const element = document.createElement("p");
    element.innerHTML = param;
    element.id = growParameters[param];
    element.classList.add("button");

    element.onclick = () => {
      if(activatedParameters.includes(param)){
        removeAllSeriesClasses(element);
        activatedParameters = activatedParameters.filter(item => item != param);
      } else
        activatedParameters.push(param);

      updateParameters();
    };
    buttonGroup.appendChild(element);
  }

  updateColors();
}

// Remove any seria-X class from an element
function removeAllSeriesClasses(element){
  for(let i = 0; i < colors.length; i++)
    element.classList.remove("series-"+i);
}

initParamButtons();

function run() {
  document.getElementById("loading").classList.add("done");
  document.getElementById("form").classList.add("active");

  const urlParam = getUrlParam("url", false);
  if(urlParam)
    loadSummary(urlParam, false);
}

// in case the document is already rendered
if (document.readyState!='loading') run();
// modern browsers
else if (document.addEventListener) document.addEventListener('DOMContentLoaded', run);
// IE <= 8
else document.attachEvent('onreadystatechange', function(){
  if (document.readyState=='complete') run();
});

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}

function getUrlParam(parameter, defaultvalue){
  var urlparameter = defaultvalue;
  if(window.location.href.indexOf(parameter) > -1){
    urlparameter = getUrlVars()[parameter];
  }
  return urlparameter;
}
